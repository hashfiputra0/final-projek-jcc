# JCC - FINAL PROJECT - KELOMPOK 7 - MY Perpus
## Anggota Kelompok
1. Hashfi Putra (CRUD Peminjaman & Deploy Aplikasi) (hashfiputra0@gmail.com)
2. Muhammad Marshal Syuhada (CRUD Buku & Demo Aplikasi) (marshalsyuhada4@gmail.com)
3. Anisa Putri Setyaningrum (CRUD Anggota) (anisaputrisetyaningrum@gmail.com)
## Tema Project
Pilihan 1 Tema Sendiri - Perpustakaan
## ERD
![ERD](public/images/ERD.PNG)
## Library/Package Di Luar Laravel
1. [DataTables](datatables.net)
2. [Nicescroll](nicescroll.areaaperta.com)
3. [SweetAlert](sweetalert.js.org)
4. [Select2](select2.org)
## Link
* Link deploy aplikasi: http://myperpus-jcc.herokuapp.com/
  > Nama: admin  
  > Email: admin@admin.com  
  > Password: admin123
* Link demo aplikasi:https://drive.google.com/drive/folders/1z2pRKoFnYJZ-lvk6z6xP-ycL4GH11Eq4?usp=sharing
