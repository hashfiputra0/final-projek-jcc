<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->middleware('auth');
Route::resource('/peminjaman', 'PeminjamanController')->middleware('auth');
Route::resource('/anggota', 'AnggotaController')->middleware('auth');
Route::resource('/buku', 'BukuController')->middleware('auth');
Route::post('/buku/hasilpencarian', 'BukuController@search')->middleware('auth');
Auth::routes();
