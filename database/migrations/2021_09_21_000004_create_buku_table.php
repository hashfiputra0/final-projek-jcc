<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBukuTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'buku';

    /**
     * Run the migrations.
     * @table buku
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('judul');
            $table->bigInteger('isbn');
            $table->string('pengarang');
            $table->string('penerbit');
            $table->smallInteger('tahun_terbit');
            $table->integer('stock');
            $table->text('deskripsi');
            $table->string('cover');

            $table->unique(["isbn"], 'isbn_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
