<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeminjamanTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'peminjaman';

    /**
     * Run the migrations.
     * @table peminjaman
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('buku_id')->unsigned();
            $table->integer('anggota_id')->unsigned();
            $table->integer('petugas_id')->unsigned();
            $table->date('tanggal_pinjam');
            $table->date('tanggal_kembali');
            $table->enum('status', ['dipinjam', 'selesai']);

            $table->index(["anggota_id"], 'fk_peminjaman_anggota_idx');

            $table->index(["buku_id"], 'fk_peminjaman_buku1_idx');

            $table->index(["petugas_id"], 'fk_peminjaman_users1_idx');


            $table->foreign('anggota_id', 'fk_peminjaman_anggota_idx')
                ->references('id')->on('anggota')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('buku_id', 'fk_peminjaman_buku1_idx')
                ->references('id')->on('buku')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('petugas_id', 'fk_peminjaman_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
