@extends('layouts.base')

@section('title') Edit Buku @endsection
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Daftar Buku</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/buku/{{$buku->id}}" method="post" enctype="multipart/form-data" id=buku>
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="judul">Judul Buku</label>
                            <input type="text" class="form-control" id="judul" placeholder="Judul Buku" name='judul' value="{{old('judul', $buku->judul)}}">
                            @error('judul')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="isbn">ISBN</label>
                            <input type="number" class="form-control" id="isbn" placeholder="ISBN" min="1" name="isbn" value="{{old('isbn', $buku->isbn)}}">
                            @error('isbn')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="pengarang">Pengarang</label>
                            <input type="text" class="form-control" id="pengarang" placeholder="Pengarang" name="pengarang" value="{{old('pengarang', $buku->pengarang)}}">
                            @error('pengarang')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="penerbit">Penerbit</label>
                            <input type="text" class="form-control" id="penerbit" placeholder="Penerbit" name="penerbit" value="{{old('penerbit', $buku->penerbit)}}">
                            @error('penerbit')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="tahun_terbit">Tahun Terbit</label>
                            <input type="number" class="form-control" id="tahun_terbit" placeholder="tahun" min="1800" name="tahun_terbit" value="{{old('tahun_terbit', $buku->tahun_terbit)}}">
                            @error('tahun_terbit')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="stock">Stock</label>
                            <input type="number" class="form-control" id="stock" placeholder="Stock" min="0" name="stock" value="{{old('stock', $buku->stock)}}">
                            @error('stock')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="deskripsit">Deksripsi</label>
                            <textarea style="resize: none" form="buku" type="text" class="form-control" id="deskripsi" placeholder="Deskripsi" name="deskripsi" rows="10">{{old('deskripsi', $buku->deskripsi)}}</textarea>
                            @error('deskripsi')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="cover">Cover Image</label>
                            <p><img src="{{asset('/storage/images/covers/'.$buku->cover)}}" class="img-thumbnail" width="200"></p>
                            <input type="file" class="form-control-file" id="cover" placeholder="Cover Image" name="cover" value="{{old('cover', $buku->cover)}}" accept="image/*">
                            @error('cover')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer d-inline-flex">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a class="btn btn-danger ml-4" href="/buku">Batal</a>
                        </div>
                    </form>
                </div>
    </div>
</div>
@endsection