@extends('layouts.base')

@section('title') Daftar Buku @endsection
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="icofont icofont-book bg-c-blue"></i>
                                <div class="d-inline">
                                    <h4>Buku</h4>
                                    <span>Lihat Daftar, Edit, Hapus, dan Tambah Koleksi Buku</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="/">
                                            <i class="icofont icofont-home"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-header bg-c-blue align-middle" style="padding-right: 0px;">
                    <div class="container d-inline">
                        <div class="row justify-content-between d-flex">
                            <div class="col-sm">
                                <h4 class="text-white mt-2">Koleksi Buku</h4>
                            </div>
                            <div class="col-4">
                                <form class="form-inline" id="search" action="/buku/hasilpencarian" method="post">
                                    @csrf
                                    <input class="form-control mr-sm-2" type="search" placeholder="Search"
                                        aria-label="Search" name="search">
                                    <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-block table-border-style text-center">
                    <div class="table-responsive">
                        @if(session('success'))
                        <div class="alert alert-success">
                            {{session('success')}}
                        </div>
                        @endif
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Judul</th>
                                    <th class="text-center">ISBN</th>
                                    <th class="text-center">Pengarang</th>
                                    <th class="text-center">Penerbit</th>
                                    <th class="text-center">Tahun Terbit</th>
                                    <th class="text-center">Stock</th>
                                    <th colspan="3" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($buku as $key => $buku)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$buku->judul}}</td>
                                    <td>{{$buku->isbn}}</td>
                                    <td>{{$buku->pengarang}}</td>
                                    <td>{{$buku->penerbit}}</td>
                                    <td>{{$buku->tahun_terbit}}</td>
                                    <td>{{$buku->stock}}</td>
                                    <td>
                                        <a class="btn btn-info btn-sm" href="/buku/{{$buku->id}}">Detail</a>
                                    </td>
                                    <td>
                                        <a class="btn btn-warning btn-sm" href="/buku/{{$buku->id}}/edit">Edit</a>
                                    </td>
                                    <td>
                                        <form action="/buku/{{$buku->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" value="hapus" name="hapus"
                                                class="btn btn-sm btn-danger">
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="12" class="text-center">Tidak Ada Buku Terdaftar</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <hr>
                        <a class="btn btn-primary m-3 float-right" href="/buku/create">Tambah Buku</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection