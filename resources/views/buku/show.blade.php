@extends('layouts.base')

@section('title') Buku No. {{ $buku->id }} @endsection
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="col-sm-12 col-sm-offset-1">
                <div class="card bg-secondary align-middle">
                    <div class="post">
                        <div class="post-thumbnail">
                            <img src="{{asset('/storage/images/covers/'.$buku->cover)}}" alt="Blog-post Thumbnail" class="rounded float-left m-3" width="500px" height="500px">
                        </div>
                        <div class="card-block post-header font-alt">
                            <br><br>
                            <h1 style="font-size: 80px" class="post-title text-white">{{$buku->judul}}</h1>    
                            <div class="post-meta text-white">
                                <p style="font-size: 20px">|By {{$buku->pengarang}}|</p>
                                <table class="table table-sm" style="width: 500px">
                                    <br><br><br>
                                    <tr>
                                        <th>ISBN</th>
                                        <td>{{$buku->isbn}}</td>
                                      </tr>
                                      <tr>
                                        <th>Penerbit</th>
                                        <td>{{$buku->penerbit}}</td>
                                      </tr>
                                      <tr>
                                        <th>Tahun Terbit</th>
                                        <td>{{$buku->tahun_terbit}}</td>
                                      </tr>
                                      <tr>
                                        <th>Stock</th>
                                        <td>{{$buku->stock}}</td>
                                      </tr>
                                  </table>
                            </div>
                        </div>
                        <br><br><br>
                        <div class="card bg-info m-4">
                        <div class="post-entry text-dark m-3">
                            <hr color="white">
                            <h3>Deskripsi</h3>
                            <hr color="white">
                            <p>{{$buku->deskripsi}}</p>
                        </div>
                        <a class="btn btn-warning btn-sm mr-3 ml-3 mb-1" href="/buku/{{$buku->id}}/edit">Edit</a>
                        <a class="btn btn-success btn-sm mr-3 ml-3 mb-3" href="/buku">Kembali Ke Koleksi</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection