@extends('layouts.base')

@section('title') Edit Peminjaman No. {{ $id->id }} @endsection
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">

                <!-- Page-header start -->
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="icofont icofont-file-code bg-c-pink"></i>
                                <div class="d-inline">
                                    <h4>Edit Peminjaman</h4>
                                    <span>mengedit data peminjaman buku yang ada</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="/">
                                            <i class="icofont icofont-home"></i>
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/peminjaman">Peminjaman</a>
                                    </li>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/peminjaman/{{$id->id}}/edit">Edit
                                            Peminjaman No. {{$id->id}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page-body start -->
                <div class="page-body">

                    <!-- Form card start -->
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Masukkan perubahan data peminjaman</h4>
                            <form action="/peminjaman/{{$id->id}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Buku</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Masukkan Data Buku Seperti Nomor ISBN Buku"
                                            data-allow-clear="1" name="buku_id" id="buku_id"
                                            class="select2 form-control @error('buku_id') is-invalid @enderror">
                                            <option></option>
                                            @foreach($buku as $value)
                                            @if ($value->id == $id->buku_id)
                                            <option selected value="{{ $value->id }}">ISBN {{ $value->isbn }} -
                                                {{ $value->judul }} ({{ $value->tahun_terbit }}) |
                                                {{ $value->pengarang }} - {{ $value->penerbit }}
                                            </option>
                                            @else
                                            <option value="{{ $value->id }}">ISBN {{ $value->isbn }} -
                                                {{ $value->judul }} ({{ $value->tahun_terbit }}) |
                                                {{ $value->pengarang }} - {{ $value->penerbit }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @error('buku_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Anggota</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Masukkan Data Anggota Seperti Nama Anggota"
                                            data-allow-clear="1" name="anggota_id" id="anggota_id"
                                            class="select2 form-control @error('anggota_id') is-invalid @enderror">
                                            <option></option>
                                            @foreach($anggota as $value)
                                            @if ($value->id == $id->anggota_id)
                                            <option selected value="{{ $value->id }}">
                                                {{ $value->nama }}
                                                @if ($value->jenis_kelamin == 'pria')
                                                (L) -
                                                @else
                                                (P) -
                                                @endif
                                                {{ $value->nohp }} | {{ $value->alamat }}
                                            </option>
                                            @else
                                            <option value="{{ $value->id }}">{{ $value->nama }}
                                                @if ($value->jenis_kelamin == 'pria')
                                                (L) -
                                                @else
                                                (P) -
                                                @endif
                                                {{ $value->nohp }} | {{ $value->alamat }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @error('anggota_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Pinjam</label>
                                    <div class="col-sm-10">
                                        <input type="date"
                                            class="form-control @error('tanggal_pinjam') is-invalid @enderror"
                                            name="tanggal_pinjam" id="tanggal_pinjam" value="{{$id->tanggal_pinjam}}">
                                        @error('tanggal_pinjam')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Kembali</label>
                                    <div class="col-sm-10">
                                        <input type="date"
                                            class="form-control @error('tanggal_kembali') is-invalid @enderror"
                                            name="tanggal_kembali" id="tanggal_kembali"
                                            value="{{$id->tanggal_kembali}}">
                                        @error('tanggal_kembali')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select name="status" id="status"
                                            class="select2-simple form-control @error('status') is-invalid @enderror">
                                            <option value="dipinjam" {{$id->status == 'dipinjam' ? "selected" : ""}}>
                                                Masih Dipinjam
                                            </option>
                                            <option value="selesai" {{$id->status == 'selesai' ? "selected" : ""}}>
                                                Sudah Dikembalikan
                                            </option>
                                        </select>
                                        @error('status')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Ubah</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Form card end -->

            </div>
            <!-- Page-body end -->

        </div>
        <div id="styleSelector">

        </div>
    </div>
    <!-- Main-body end -->

</div>
</div>
@endsection

@push('css-select2-bootstrap4')
<!-- CSS Select2 Bootstrap 4 -->
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2-bootstrap4.min.css') }}">
@endpush

@push('js-select2-bootstrap4')
<!-- Js Select2 Bootstrap 4 -->
<script src="{{ asset('select2-bootstrap4/dist/js/select2.min.js') }}"></script>
<script>
    $(function () {
        $('.select2').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
                closeOnSelect: !$(this).attr('multiple')
            });
        });
        $('.select2-simple').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                minimumResultsForSearch: -1
            });
        });
    });
</script>
@endpush