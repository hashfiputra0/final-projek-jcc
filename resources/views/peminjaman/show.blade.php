@extends('layouts.base')

@section('title') Peminjaman No. {{$peminjaman->id}} @endsection
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <!-- Main-body start -->
        <div class="main-body" id="main-body">
            <div class="page-wrapper">

                <!-- Page-header start -->
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="icofont icofont-search bg-c-pink"></i>
                                <div class="d-inline">
                                    <h4>Melihat Peminjaman Yang Ada</h4>
                                    <span>melihat lebih detail peminjaman buku yang ada</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="/">
                                            <i class="icofont icofont-home"></i>
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/peminjaman">Peminjaman</a>
                                    </li>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/peminjaman/show/">Peminjaman No.
                                            {{$peminjaman->id}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page-body start -->
                <div class="page-body">

                    <!-- Form card start -->
                    <div class="card">
                        <div class="card-block typography">
                            <h4 class="sub-title">Peminjaman Nomor {{$peminjaman->id}}</h4>
                            <h3>
                                Buku {{ $peminjaman->buku->judul }} ({{ $peminjaman->buku->tahun_terbit }})<br>
                                <small>ISBN {{ $peminjaman->buku->isbn }}<br></small>
                                <small>Oleh {{ $peminjaman->buku->pengarang }} |
                                    {{ $peminjaman->buku->penerbit }}</small>
                            </h3>
                            <h5>
                                Dipinjam Oleh<br>
                                <small>{{ $peminjaman->anggota->nama }}<br></small>
                                @if ($peminjaman->anggota->jenis_kelamin == 'pria')
                                <small>Laki-laki<br></small>
                                @else
                                <small>Perempuan<br></small>
                                @endif
                                <small>{{ $peminjaman->anggota->nohp }}<br></small>
                                <small>{{ $peminjaman->anggota->alamat }}<br></small>
                            </h5>
                            <h5>
                                Petugas Transaksi Peminjaman<br>
                                <small>{{ $peminjaman->user->name }}<br></small>
                            </h5>
                            <h5>
                                Pada Tanggal<br>
                                <small>{{ $peminjaman->tanggal_pinjam }}<br></small>
                                Akan Dikembalikan Paling Telat<br>
                                <small>{{ $peminjaman->tanggal_kembali }}</small>
                            </h5>
                            <h5>
                                Status<br>
                                @if ($peminjaman->status == 'dipinjam')
                                <small>Belum Dikembalikan</small>
                                @else
                                <small>Sudah Dikembalikan</small>
                                @endif
                            </h5>
                        </div>
                    </div>
                </div>
                <!-- Form card end -->

            </div>
            <!-- Page-body end -->

        </div>
    </div>
    <!-- Main-body end -->

</div>
</div>
@endsection