@extends('layouts.base')

@section('title') Peminjaman @endsection
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">

                <!-- Page-header start -->
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="icofont icofont-book-mark bg-c-pink"></i>
                                <div class="d-inline">
                                    <h4>
                                        Peminjaman
                                    </h4>
                                    <span>melihat, edit, hapus riwayat peminjaman. dan juga tambah peminjaman
                                        baru</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="/">
                                            <i class="icofont icofont-home"></i>
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/peminjaman">Peminjaman</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page-body start -->
                <div class="page-body">

                    <!-- Basic table card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Peminjaman Yang Ada</h5>
                            <div class="card-header-right">
                                <ul class="card-option">
                                    <li><i class="icofont icofont-maximize full-card"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-block table-border-style">
                            <div class="table-responsive">
                                <table class="table" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Buku</th>
                                            <th>Nama Anggota</th>
                                            <th>Petugas Transaksi</th>
                                            <th>Tanggal Pinjam</th>
                                            <th>Tanggal Kembali</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($peminjaman as $key=>$value)
                                        <tr>
                                            <td>{{$key + 1}}</th>
                                            <td>{{$value->buku->judul}}</td>
                                            <td>{{$value->anggota->nama}}</td>
                                            <td>{{$value->user->name}}</td>
                                            <td>{{$value->tanggal_pinjam}}</td>
                                            <td>{{$value->tanggal_kembali}}</td>
                                            @if ($value->status == 'dipinjam')
                                            <td>Masih Dipinjam</td>
                                            @else
                                            <td>Sudah Dikembalikan</td>
                                            @endif
                                            <td>
                                                <a href="/peminjaman/{{$value->id}}" class="btn btn-info">
                                                    <i class="icofont icofont-search"></i>
                                                </a>
                                                <a href="/peminjaman/{{$value->id}}/edit" class="btn btn-primary">
                                                    <i class="icofont icofont-edit"></i>
                                                </a>
                                                <form action="/peminjaman/{{$value->id}}" method="POST" id="delete">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" form="delete" class="btn btn-danger mt-1"
                                                        style="width: 162px">
                                                        Hapus
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td class="text-center" colspan="7">Tidak ada data peminjaman</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Basic table card end -->

                </div>
                <!-- Page-body end -->

            </div>
        </div>
        <!-- Main-body end -->

    </div>
</div>
@endsection

@push('css-datatables-bootstrap4')
<!-- CSS DataTables Bootstrap 4 -->
<link rel="stylesheet" href="{{ asset('datatables.net/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('datatables.net/css/fixedHeader.bootstrap4.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('datatables.net/css/responsive.bootstrap4.min.css') }}">
@endpush

@push('js-datatables-bootstrap4')
<!-- JS DataTables Bootstrap 4 -->
<script src="{{ asset('datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables.net/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('datatables.net/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('datatables.net/js/fixedHeader.bootstrap4.min.js') }}"></script>
<script src="{{ asset('datatables.net/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('datatables.net/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            responsive: true,
            fixedHeader: true,
            "pagingType": "full_numbers"
        });
    });
</script>
@endpush