@extends('layouts.base')

@section('title') Peminjaman Baru @endsection
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">

                <!-- Page-header start -->
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="icofont icofont-file-code bg-c-pink"></i>
                                <div class="d-inline">
                                    <h4>Peminjaman Baru</h4>
                                    <span>tambah peminjaman buku baru</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="/">
                                            <i class="icofont icofont-home"></i>
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/peminjaman">Peminjaman</a>
                                    </li>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/peminjaman/create">Peminjaman Baru</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page-body start -->
                <div class="page-body">

                    <!-- Form card start -->
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Masukkan data peminjaman</h4>
                            <form action="/peminjaman" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Buku</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Masukkan Data Buku Seperti Nomor ISBN Buku"
                                            data-allow-clear="1" name="buku_id" id="buku_id"
                                            class="form-control @error('buku_id') is-invalid @enderror">
                                            <option></option>
                                            @foreach($buku as $value)
                                            <option value="{{ $value->id }}">ISBN {{ $value->isbn }} -
                                                {{ $value->judul }} ({{ $value->tahun_terbit }}) |
                                                {{ $value->pengarang }} - {{ $value->penerbit }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('buku_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Anggota</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Masukkan Data Anggota Seperti Nama Anggota"
                                            data-allow-clear="1" name="anggota_id" id="anggota_id"
                                            class="form-control @error('anggota_id') is-invalid @enderror">
                                            <option></option>
                                            @foreach($anggota as $value)
                                            <option value="{{ $value->id }}">{{ $value->nama }}
                                                @if ($value->jenis_kelamin == 'pria')
                                                (L) -
                                                @else
                                                (P) -
                                                @endif
                                                {{ $value->nohp }} | {{ $value->alamat }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('anggota_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Pinjam</label>
                                    <div class="col-sm-10">
                                        <input type="date"
                                            class="form-control @error('tanggal_pinjam') is-invalid @enderror"
                                            name="tanggal_pinjam" id="tanggal_pinjam">
                                        @error('tanggal_pinjam')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Kembali</label>
                                    <div class="col-sm-10">
                                        <input type="date"
                                            class="form-control @error('tanggal_kembali') is-invalid @enderror"
                                            name="tanggal_kembali" id="tanggal_kembali">
                                        @error('tanggal_kembali')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Form card end -->

            </div>
            <!-- Page-body end -->

        </div>
    </div>
    <!-- Main-body end -->

</div>
@endsection

@push('css-select2-bootstrap4')
<!-- CSS Select2 Bootstrap 4 -->
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2-bootstrap4.min.css') }}">
@endpush

@push('js-select2-bootstrap4')
<!-- Js Select2 Bootstrap 4 -->
<script src="{{ asset('select2-bootstrap4/dist/js/select2.min.js') }}"></script>
<script>
    $(function () {
        $('select').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
                closeOnSelect: !$(this).attr('multiple'),
            });
        });
    });
</script>
@endpush