@extends('layouts.base')

@section('title') Anggota @endsection
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">

                <!-- Page-header start -->
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="icofont icofont-users bg-c-green"></i>
                                <div class="d-inline">
                                    <h4>Anggota</h4>
                                    <span>Melihat, edit, hapus riwayat anggota. dan juga tambah anggota
                                        baru</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="/">
                                            <i class="icofont icofont-home"></i>
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/anggota">Anggota</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page-body start -->
                <div class="page-body">

                    <!-- Basic table card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Anggota Yang Ada</h5>
                            <div class="card-header-right">
                                <a href="{{route('anggota.create')}}" class="btn btn-success">Tambah Anggota</a>
                            </div>
                        </div>
                        <div class="card-block table-border-style">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama</th>
                                            <th>Jenis Kelamin</th>
                                            <th>NO HP</th>
                                            <th>Alamat</th>
                                            <th>Tanggal Gabung</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($anggota as $key=>$value)
                                        <tr>
                                            <td>{{$key + 1}}</th>
                                            <td>{{$value->nama}}</td>
                                            <td>{{$value->jenis_kelamin}}</td>
                                            <td>{{$value->nohp}}</td>
                                            <td>{{$value->alamat}}</td>
                                            <td>{{$value->tanggal_gabung}}</td>
                                            <td>
                                                <a href="/anggota/{{$value->id}}" class="btn btn-info">Lihat</a>
                                                <a href="/anggota/{{$value->id}}/edit"
                                                    class="btn btn-primary my-1">Ubah</a>
                                                <form action="/anggota/{{$value->id}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <input type="submit" class="btn btn-danger" value="Hapus">
                                                </form>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td class="text-center" colspan="7">Tidak ada data</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Basic table card end -->

                </div>
                <!-- Page-body end -->

            </div>
            <div id="styleSelector">

            </div>
        </div>
        <!-- Main-body end -->

    </div>
</div>
@endsection
