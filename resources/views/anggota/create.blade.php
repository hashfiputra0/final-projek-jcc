@extends('layouts.base')

@section('title') Anggota Baru @endsection
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">

                <!-- Page-header start -->
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="icofont icofont-file-code bg-c-green"></i>
                                <div class="d-inline">
                                    <h4>Anggota Baru</h4>
                                    <span>Tambah Anggota Baru</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="/">
                                            <i class="icofont icofont-home"></i>
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/anggota">Anggota</a>
                                    </li>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/anggota/create">Anggota Baru</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page-body start -->
                <div class="page-body">

                    <!-- Form card start -->
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Masukkan data anggota</h4>
                            <form action="/anggota" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Lengkap</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="nama" id="nama">
                                    </div>
                                    @error('nama')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Pilih Jenis Kelamin" data-allow-clear="1"
                                            name="jenis_kelamin" id="jenis_kelamin">
                                            <option value="pria">Pria</option>
                                            <option value="wanita">Wanita</option>
                                        </select>
                                    </div>
                                    @error('jenis_kelamin')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">No HP</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="nohp" id="nohp">
                                    </div>
                                    @error('nohp')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <textarea name="alamat" id="alamat" cols="30" rows="10"
                                            class="form-control"></textarea>
                                    </div>
                                    @error('alamat')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Gabung</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" name="tanggal_gabung"
                                            id="tanggal_gabung">
                                    </div>
                                    @error('tanggal_gabung')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Form card end -->

            </div>
            <!-- Page-body end -->

        </div>
        <div id="styleSelector">

        </div>
    </div>
    <!-- Main-body end -->

</div>
@endsection

@push('css-select2-bootstrap4')
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('select2-bootstrap4/dist/css/select2-bootstrap4.min.css') }}">
@endpush

@push('js-select2-bootstrap4')
<script src="{{ asset('select2-bootstrap4/dist/js/select2.min.js') }}"></script>
<script>
    $(function () {
        $('select').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass(
                    'w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
                closeOnSelect: !$(this).attr('multiple'),
            });
        });
    });

</script>
@endpush
