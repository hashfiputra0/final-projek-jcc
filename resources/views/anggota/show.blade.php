@extends('layouts.base')

@section('title') Anggota No. {{$anggota->id}} @endsection
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">

                <!-- Page-header start -->
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="icofont icofont-search bg-c-green"></i>
                                <div class="d-inline">
                                    <h4>Melihat anggota Yang Ada</h4>
                                    <span>Melihat lebih detail anggota yang ada</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="/">
                                            <i class="icofont icofont-home"></i>
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/anggota">Anggota</a>
                                    </li>
                                    </li>
                                    <li class="breadcrumb-item"><a href="/anggota/show/">Anggota No.
                                            {{$anggota->id}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-header end -->

                <!-- Page-body start -->
                <div class="page-body">

                    <!-- Form card start -->
                    <div class="card">
                        <div class="card-block typography">
                            <h4 class="sub-title">Anggota Nomor {{$anggota->id}}</h4>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Lengkap</label>
                                <div class="col-sm-10">
                                    <label>{{$anggota->nama}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                <div class="col-sm-10">
                                    <label>{{$anggota->jenis_kelamin}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">No HP</label>
                                <div class="col-sm-10">
                                    <label>{{$anggota->nohp}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-10">
                                    <label>{{$anggota->alamat}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Tanggal Gabung</label>
                                <div class="col-sm-10">
                                    <label>{{$anggota->tanggal_gabung}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Form card end -->

            </div>
            <!-- Page-body end -->

        </div>
        <div id="styleSelector">

        </div>
    </div>
    <!-- Main-body end -->

</div>
</div>
@endsection
