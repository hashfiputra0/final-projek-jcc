<nav class="pcoded-navbar">
    <div class="sidebar_toggle"><a href="/"><i class="icon-close icons"></i></a></div>
    <div class="pcoded-inner-navbar main-menu">
        <ul class="pcoded-item pcoded-left-item">
            <li>
                <a href="/">
                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Dashboard</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <div class="pcoded-navigatio-lavel" data-i18n="nav.category.forms">CRUD</div>
        <ul class="pcoded-item pcoded-left-item">
            <li>
                <a href="/buku">
                    <span class="pcoded-micon"><i class="ti-book"></i><b>BKU</b></span>
                    <span class="pcoded-mtext" data-i18n="nav.form-components.main">Buku</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="pcoded-hasmenu">
                <a href="#">
                    <span class="pcoded-micon"><i class="ti-bookmark"></i><b>PMJ</b></span>
                    <span class="pcoded-mtext" data-i18n="nav.form-components.main">Peminjaman</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="/peminjaman">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">Peminjaman Yang Ada</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="/peminjaman/create">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext" data-i18n="nav.basic-components.alert">Tambah Peminjaman
                                Baru</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/anggota">
                    <span class="pcoded-micon"><i class="ti-user"></i><b>AGT</b></span>
                    <span class="pcoded-mtext" data-i18n="nav.form-components.main">Anggota</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </div>
</nav>
