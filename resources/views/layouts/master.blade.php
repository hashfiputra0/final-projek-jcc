<!DOCTYPE html>
<html lang="en">

<head>
    <title>MY Perpus | Dashboard</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="CodedThemes">
    <meta name="keywords"
        content=" Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="CodedThemes">
    <!-- Favicon icon -->
    <link rel="icon" href="{{ asset('guruable-master/assets/images/favicon.ico') }}" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('guruable-master/assets/css/bootstrap/css/bootstrap.min.css') }}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('guruable-master/assets/icon/themify-icons/themify-icons.css') }}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{ asset('guruable-master/assets/icon/icofont/css/icofont.css') }}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('guruable-master/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('guruable-master/assets/css/jquery.mCustomScrollbar.css') }}">
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <!-- Navbar -->
            @include('layouts.navbar')
            <!-- /.navbar -->

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">

                    <!-- Sidebar -->
                    @include('layouts.sidebar')
                    <!-- /.sidebar -->

                    <!-- Main Content -->
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">

                                    <div class="page-body">
                                        <div class="row">
                                            <!-- card1 start -->
                                            <div class="col-md-6 col-xl-3">
                                                <div class="card widget-card-1">
                                                    <div class="card-block-small">
                                                        <i class="icofont icofont-book bg-c-blue card1-icon"></i>
                                                        <span class="text-c-blue f-w-600">Buku</span>
                                                        <h4>{{$buku}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- card1 end -->
                                            <!-- card1 start -->
                                            <div class="col-md-6 col-xl-3">
                                                <div class="card widget-card-1">
                                                    <div class="card-block-small">
                                                        <i class="icofont icofont-book-mark bg-c-pink card1-icon"></i>
                                                        <span class="text-c-pink f-w-600">Peminjaman</span>
                                                        <h4>{{$peminjaman}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- card1 end -->
                                            <!-- card1 start -->
                                            <div class="col-md-6 col-xl-3">
                                                <div class="card widget-card-1">
                                                    <div class="card-block-small">
                                                        <i class="icofont icofont-users bg-c-green card1-icon"></i>
                                                        <span class="text-c-green f-w-600">Anggota</span>
                                                        <h4>{{$anggota}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- card1 end -->
                                            <!-- card1 start -->
                                            <div class="col-md-6 col-xl-3">
                                                <div class="card widget-card-1">
                                                    <div class="card-block-small">
                                                        <i class="icofont icofont-user-alt-1 bg-c-yellow card1-icon"></i>
                                                        <span class="text-c-yellow f-w-600">Petugas</span>
                                                        <h4>{{$petugas}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- card1 end -->
                                        </div>
                                    </div>
                                </div>

                                <div id="styleSelector">

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.main-content -->

                </div>
            </div>
        </div>
    </div>

    <!-- Required Jquery -->
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/popper.js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/bootstrap/js/bootstrap.min.js') }}">
    </script>
    <!-- modernizr js -->
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/modernizr/modernizr.js') }}"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="{{ asset('guruable-master/assets/pages/dashboard/custom-dashboard.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/script.js') }}"></script>
    <script src="{{ asset('guruable-master/assets/js/pcoded.min.js') }}"></script>
    <script src="{{ asset('guruable-master/assets/js/demo-12.js') }}"></script>
</body>

</html>