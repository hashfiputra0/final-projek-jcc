<!DOCTYPE html>
<html lang="en">

<head>
    <title>MY Perpus | @yield('title')</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="CodedThemes">
    <meta name="keywords"
        content=" Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="CodedThemes">
    <!-- Favicon icon -->
    <link rel="icon" href="{{ asset('guruable-master/assets/images/favicon.ico') }}" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('guruable-master/assets/css/bootstrap/css/bootstrap.min.css') }}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('guruable-master/assets/icon/themify-icons/themify-icons.css') }}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{ asset('guruable-master/assets/icon/icofont/css/icofont.css') }}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('guruable-master/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('guruable-master/assets/css/jquery.mCustomScrollbar.css') }}">
    @stack('css-select2-bootstrap4')
    @stack('css-datatables-bootstrap4')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <!-- Navbar -->
            @include('layouts.navbar')
            <!-- /.navbar -->

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper" id="main-body">

                    <!-- Sidebar -->
                    @include('layouts.sidebar')
                    <!-- /.sidebar -->

                    <!-- Main Content -->
                    @yield('content')
                    <!-- /.main-content -->

                </div>
            </div>
        </div>
    </div>

    <!-- Required Jquery -->
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/popper.js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/bootstrap/js/bootstrap.min.js') }}">
    </script>
    <!-- modernizr js -->
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/modernizr/modernizr.js') }}"></script>
    <!-- Custom JS GURUable Template-->
    <script type="text/javascript" src="{{ asset('guruable-master/assets/pages/dashboard/custom-dashboard.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('guruable-master/assets/js/script.js') }}"></script>
    <script src="{{ asset('guruable-master/assets/js/pcoded.min.js') }}"></script>
    <script src="{{ asset('guruable-master/assets/js/demo-12.js') }}"></script>
    @stack('js-select2-bootstrap4')
    @stack('js-datatables-bootstrap4')
    <!-- NiceScroll JS -->
    <script type="text/javascript" src="{{ asset('nicescroll-3.7.6/dist/jquery.nicescroll.min.js') }}"></script>
    <script>
        $(function() {  
            $("body").niceScroll({
                cursorcolor: "#303549",
                cursorborder: "none",
                cursorwidth: "10px",
                cursorborderradius: "2px",
                scrollspeed: 100,
            });
        });
    </script>
    <!-- SweetAlert JS -->
    <script type="text/javascript" src="{{ asset('sweetalert-2.1.2/js/sweetalert.min.js') }}"></script>
    <script>
        @if (session('status'))
            swal({
                title: '{{ session('status')}}',
                icon: '{{ session('statusCode')}}',
                button: "Ok"
            });
        @endif
    </script>
</body>

</html>