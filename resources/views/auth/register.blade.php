@extends('layouts.app')

@section('title')
    Register
@endsection
@section('content')
<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- Authentication card start -->
                <div class="signup-card card-block auth-body mr-auto ml-auto">
                    <form class="md-float-material" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="text-center">
                            <img src="{{ asset('guruable-master/assets/images/auth/logo.png') }}" alt="logo.png">
                        </div>
                        <div class="auth-box">
                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <h3 class="text-center txt-primary">Daftar</h3>
                                </div>
                            </div>
                            <hr />

                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                                placeholder="Masukkan Nama">
                            <span class="md-line"></span>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror


                            <input id="email" type="email"
                                class="form-control @error('email') is-invalid @enderror  my-3" name="email"
                                value="{{ old('email') }}" required autocomplete="email" placeholder="Masukkan Email">
                            <span class="md-line"></span>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                autocomplete="new-password" placeholder="Masukkan Password">
                            <span class="md-line"></span>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <input id="password-confirm" type="password" class="form-control my-3"
                                name="password_confirmation" required autocomplete="new-password"
                                placeholder="Konfirmasi Password">
                            <span class="md-line"></span>
                            <div class="col-xs-12 forgot-phone text-right">
                                <a href="{{ route('login') }}" class="text-right f-w-600 text-inverse"> Sudah Punya
                                    Akun?</a>
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12">
                                    <button type="submit"
                                        class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Daftar
                                        Sekarang</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- end of form -->
                </div>
                <!-- Authentication card end -->
            </div>
            <!-- end of col-sm-12 -->
        </div>
        <!-- end of row -->
    </div>
    <!-- end of container-fluid -->
</section>
@endsection