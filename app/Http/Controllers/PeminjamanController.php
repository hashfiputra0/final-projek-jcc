<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Peminjaman;
use App\Buku;
use App\Anggota;
use Illuminate\Support\Facades\Session;
use DB;

class PeminjamanController extends Controller
{
    // Menampilkan list data peminjaman.
    public function index()
    {
        $peminjaman = Peminjaman::all();
        return view('peminjaman.index', compact('peminjaman'));
    }

    // Menampilkan form untuk membuat data peminjaman baru.
    public function create()
    {
        $buku = Buku::all();
        $anggota = Anggota::all();
        return view('peminjaman.create', compact('buku', 'anggota'));
    }

    // Menyimpan data baru ke tabel Peminjaman
    public function store(Request $request)
    {
        $this->validate($request, [
            'buku_id' => 'required',
            'anggota_id' => 'required',
            'tanggal_pinjam' => 'required|date',
            'tanggal_kembali' => 'required|date'
        ],
        // Pesan error validate
        [
            'buku_id.required' => 'Data buku harus diisi.',
            'anggota_id.required' => 'Data anggota harus diisi.',
            'tanggal_pinjam.required' => 'Tanggal pinjam harus diisi.',
            'tanggal_pinjam.date' => 'Data harus berupa tanggal.',
            'tanggal_kembali.required' => 'Tanggal kembali harus diisi.',
            'tanggal_kembali.date' => 'Data harus berupa tanggal.'
        ]);

        Peminjaman::create([
            'buku_id' => $request->buku_id,
            'anggota_id' => $request->anggota_id,
            'petugas_id' => Auth::user()->id,
            'tanggal_pinjam' => $request->tanggal_pinjam,
            'tanggal_kembali' => $request->tanggal_kembali,
            'status' => 'dipinjam'
        ]);

        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/peminjaman')->with('status', 'Peminjaman Berhasil Ditambahkan!');
    }

    // Menampilkan detail data peminjaman dengan id tertentu
    public function show($id)
    {
        $peminjaman = Peminjaman::find($id);
        return view('peminjaman.show', compact('peminjaman'));
    }

    // 	Menampilkan form untuk edit data peminjaman dengan id tertentu
    public function edit($id)
    {
        $id = Peminjaman::find($id);
        $buku = Buku::all();
        $anggota = Anggota::all();
        return view('peminjaman.edit', compact('id', 'buku', 'anggota'));
    }

    // 	Menyimpan perubahan data peminjaman (update) untuk id tertentu
    public function update(Request $request, $id)
    {
        $request->validate([
            'buku_id' => 'required',
            'anggota_id' => 'required',
            'tanggal_pinjam' => 'required|date',
            'tanggal_kembali' => 'required|date',
            'status' => 'required'
        ],
        // Pesan error vaidate
        [
            'buku_id.required' => 'Data buku harus diisi.',
            'anggota_id.required' => 'Data anggota harus diisi.',
            'tanggal_pinjam.required' => 'Tanggal pinjam harus diisi.',
            'tanggal_pinjam.date' => 'Data harus berupa tanggal.',
            'tanggal_kembali.required' => 'Tanggal kembali harus diisi.',
            'tanggal_kembali.date' => 'Data harus berupa tanggal.',
            'status.required' => 'Status harus diisi.'
        ]);

        $peminjaman = Peminjaman::find($id);
        $peminjaman->buku_id = $request->buku_id;
        $peminjaman->anggota_id = $request->anggota_id;
        $peminjaman->petugas_id = Auth::user()->id;
        $peminjaman->tanggal_pinjam = $request->tanggal_pinjam;
        $peminjaman->tanggal_kembali = $request->tanggal_kembali;
        $peminjaman->status = $request->status;
        $peminjaman->update();

        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/peminjaman')->with('status', 'Peminjaman Berhasil Diubah!');
    }

    // 	Menghapus data peminjaman dengan id tertentu
    public function destroy($id)
    {
        $peminjaman = Peminjaman::find($id);
        $peminjaman->delete();

        // Sweet Alert
        Session::flash('statusCode', 'success');
        return redirect('/peminjaman')->with('status', 'Peminjaman Berhasil Dihapus!');
    }
}
