<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Anggota;

class AnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // Menampilkan list data anggota.
    public function index()
    {
        $anggota = Anggota::all();
        return view('anggota.index', compact('anggota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // Menampilkan form untuk membuat data anggota baru.
    public function create()
    {
        $anggota = Anggota::all();
        return view('anggota.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // Menyimpan data baru ke tabel Anggota
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'alamat' => 'required',
            'nohp' => 'required',
            'jenis_kelamin' => 'required',
            'tanggal_gabung' => 'required',
        ]);
        Anggota::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'nohp' => $request->nohp,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tanggal_gabung' => $request->tanggal_gabung
        ]);
        return redirect('/anggota');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // Menampilkan detail data anggota dengan id tertentu
    public function show($id)
    {
        $anggota = Anggota::find($id);
        return view('anggota.show', compact('anggota'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // 	Menampilkan form untuk edit data anggota dengan id tertentu
    public function edit($id)
    {
        $anggota = Anggota::find($id);
        return view('anggota.edit', compact('id', 'anggota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // 	Menyimpan perubahan data anggota (update) untuk id tertentu
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'nohp' => 'required',
            'jenis_kelamin' => 'required',
            'tanggal_gabung' => 'required',
        ]); 

        $anggota = Anggota::find($id);
        $anggota->nama = $request->nama;
        $anggota->alamat = $request->alamat;
        $anggota->nohp =  $request->nohp;
        $anggota->jenis_kelamin = $request->jenis_kelamin;
        $anggota->tanggal_gabung = $request->tanggal_gabung;
        $anggota->update();

        return redirect('/anggota');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // 	Menghapus data anggota dengan id tertentu
    public function destroy($id)
    {
        $anggota = Anggota::find($id);
        $anggota->delete();
        return redirect('/anggota');
    }
}
