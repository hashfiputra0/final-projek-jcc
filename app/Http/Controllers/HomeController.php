<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;
use App\Buku;
use App\Anggota;
use App\User;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $peminjaman = Peminjaman::count();
        $buku = Buku::count();
        $anggota = Anggota::count();
        $petugas = User::count();
        return view('layouts.master', compact('peminjaman', 'buku', 'anggota', 'petugas'));
    }

}
