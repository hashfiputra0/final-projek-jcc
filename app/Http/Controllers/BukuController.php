<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Buku;
use Storage;

class BukuController extends Controller
{
    public function index()
    {
        $buku = DB::table('buku')->get();
        return view('buku.index', compact('buku'));
    }
    public function create()
    {
        return view('buku.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isbn' => 'required|unique:buku|max:20',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun_terbit' => 'required',
            'stock' => 'required',
            'deskripsi' => 'required',
            'cover' => 'required'
        ]);
        if($request->hasFile('cover'))
        {
            $destination_path = 'public/images/covers';
            $cover = $request->file('cover');
            $image_name =  $cover->getClientOriginalName();
            $request->file('cover')->storeAs($destination_path, $image_name);
        }
        $query = DB::table('buku')->insert([
            'judul' => $request["judul"],
            'isbn' => $request["isbn"],
            'pengarang' => $request["pengarang"],
            'penerbit' => $request["penerbit"],
            'tahun_terbit' => $request["tahun_terbit"],
            'stock' => $request["stock"],
            'deskripsi' => $request["deskripsi"],
            'cover' => $image_name
        ]);
        return redirect('/buku')->with('success', 'Buku Berhasil Ditambahkan!');
    }
    public function show($buku_id)
    {
        $buku = DB::table('buku')->where('id', $buku_id)->first();  
        return view('buku.show', compact('buku'));
    }
    public function edit($buku_id)
    {
        $buku = DB::table('buku')->where('id', $buku_id)->first();
        return view('buku.edit', compact('buku'));
    }
    public function update($buku_id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isbn' => 'required|max:20',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun_terbit' => 'required',
            'stock' => 'required',
            'deskripsi' => 'required',
            'cover' => 'required'
        ]);
        if($request->hasFile('cover'))
        { 
            $destination_path = 'public/images/covers';
            $cover = $request->file('cover');
            $image_name =  $cover->getClientOriginalName();
            $request->file('cover')->storeAs($destination_path, $image_name);
        }
        $query = DB::table('buku')
            ->where('id', $buku_id)
            ->update([
            'judul' => $request["judul"],
            'isbn' => $request["isbn"],
            'pengarang' => $request["pengarang"],
            'penerbit' => $request["penerbit"],
            'tahun_terbit' => $request["tahun_terbit"],
            'stock' => $request["stock"],
            'deskripsi' => $request["deskripsi"],
            'cover' => $image_name
            ]);
        return redirect('/buku')->with('success', 'Data Berhasil Diperbarui!');
    }
    public function destroy($buku_id)
    {
        $query = DB::table('buku')->where('id', $buku_id)->delete();
        return redirect('/buku')->with('success', 'Data Berhasil Dihapus!');
    }
    public function search(Request $request)
    {
        if($request['search'] == '')
        {
            return  redirect('/buku');
        }
        else
        {
        $buku = DB::table('buku')
        ->where('judul', 'like', '%'.$request['search'].'%')
        ->orWhere('isbn', 'like', '%'.$request['search'].'%')
        ->orWhere('tahun_terbit', 'like', '%'.$request['search'].'%')
        ->orWhere('penerbit', 'like', '%'.$request['search'].'%')
        ->orWhere('pengarang', 'like', '%'.$request['search'].'%')
        ->get();
        return view('buku.search', compact('buku'));
        }
    }
}
