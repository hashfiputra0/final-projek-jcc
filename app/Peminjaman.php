<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = "peminjaman";
    protected $guarded = [];
    public $timestamps = false;

    // Relationship Buku
    public function buku()
    {
        return $this->belongsTo('App\Buku');
    }

    // Relationship Anggota
    public function anggota()
    {
        return $this->belongsTo('App\Anggota');
    }

    // Relationship User
    public function user()
    {
        return $this->belongsTo('App\User', 'petugas_id');
    }
}
